import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Store, Select } from '@ngxs/store';
import { IViewedConversion } from './../../models/viewed-conversion.model';
import { DelHistory, GetHistory } from 'src/app/store/conversion.actions';
declare var $;
declare var M;

@Component({
  selector: 'app-view-history',
  templateUrl: './view-history.component.html',
  styleUrls: ['./view-history.component.scss']
})
export class ViewHistoryComponent implements OnInit {

  selectedIndex;
  sub$;

  @Select(state => state.conversion) viewedConversions$: Observable<IViewedConversion[]>;

  constructor(private store: Store, private router: Router) { }

  ngOnInit() {
    $('.modal').modal();
    this.store.dispatch(new GetHistory());
  }

  viewHistory(index:number){
    this.sub$ = this.viewedConversions$.subscribe((conversions)=>{
      const conversion = conversions[index];
      this.router.navigate(['/currency-converter'], { queryParams: {...conversion} });
    });
  }

  deleteHistory(index:number){
    this.store.dispatch(new DelHistory({position:index}));
  }

  ngOnDestroy(){
    if(this.sub$!==undefined){
      this.sub$.unsubscribe();
    }  
  }

}
