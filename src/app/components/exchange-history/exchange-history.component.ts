import { Observable, forkJoin } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit, SimpleChanges, Input, SimpleChange } from '@angular/core';
import { IExchangeRate } from './../../models/exchange-rate.model';
import { environment } from 'src/environments/environment';
declare var $;

@Component({
  selector: 'app-exchange-history',
  templateUrl: './exchange-history.component.html',
  styleUrls: ['./exchange-history.component.scss']
})
export class ExchangeHistoryComponent implements OnInit {

  @Input('fromCur')fromCurrency:string;
  @Input('toCur')toCurrency:string;
  selectedDays:number = 7;
  exchangeHistories:IExchangeRate[]=[];
  stats = {
    low:0,
    high:0,
    average:0,
  }

  constructor(private http: HttpClient) { }

  ngOnInit() {
    $('select').formSelect();
  }

  ngOnChanges(changes: SimpleChanges) {
    const fromChange: SimpleChange = changes.fromCurrency;
    const toChange: SimpleChange = changes.toCurrency;
    if(fromChange.currentValue!==undefined && toChange.currentValue!==undefined){
      this.triggerChange(this.fromCurrency, this.toCurrency);
    }
  }

  onDaysChange(days:number){
    this.triggerChange(this.fromCurrency, this.toCurrency);
  }

  triggerChange(from:string, to:string){
    const rates = []
    this.getHistories(from, to).subscribe(res=>{
    this.exchangeHistories = res[0].map((e,i)=>{
        e.rate = e.rate/res[1][i].rate;
        rates.push(e.rate);
        return e;
      });
      console.log(rates);
      this.stats = {
        low: Math.min(...rates),
        high: Math.max(...rates),
        average: this.average(rates)
      }
    });
  }

  getHistories(fromCurrency:string, toCurrency:string):Observable<any[]>{
    let today = new Date()
    const endDate = today.toJSON();
    const startDate = new Date(today.setDate(today.getDate()-this.selectedDays)).toJSON();
    
    const fromObs = this.http
      .get<IExchangeRate[]>(`${environment.apiUrls.getExchangeRateHistory}${fromCurrency}&start=${startDate}&end=${endDate}`);

    const toObs = this.http
      .get<IExchangeRate[]>(`${environment.apiUrls.getExchangeRateHistory}${toCurrency}&start=${startDate}&end=${endDate}`);

    return forkJoin(fromObs, toObs);    
  }

  average = arr => arr.reduce( ( p, c ) => p + c, 0 ) / arr.length;

}
