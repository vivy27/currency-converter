import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { ActivatedRoute } from '@angular/router';
import { formatDate } from '@angular/common';
import { Store } from '@ngxs/store';
import { IExchangeRate } from './../../models/exchange-rate.model';
import { AddHistory } from './../../store/conversion.actions';
import { IViewedConversion } from './../../models/viewed-conversion.model';
import { environment } from 'src/environments/environment';
declare var M;

@Component({
  selector: 'app-currency-converter',
  templateUrl: './currency-converter.component.html',
  styleUrls: ['./currency-converter.component.scss']
})
export class CurrencyConverterComponent implements OnInit {

  public _conversion:IViewedConversion={
    date:null,
    amount:null,
    from:null,
    to:null
  }
  public fromVal;
  public toVal;
  public actualCur;
  public totalunit;
  public fromValue;
  public toValue;
  public fromCurrency;
  public toCurrency;
  httpSub;
  routeSub;
  currencies: { data: { [key: string]: string } };
  locale:string = 'en-US';
  constructor(private http: HttpClient, private store: Store, private route: ActivatedRoute) { }

  ngOnInit() {
    this.http
      .get<IExchangeRate[]>(environment.apiUrls.getCurrencies)
      .subscribe(
        data => {
          this.setCurrencies(data);
          this.setHistory();
        },
        error => {
          console.log("Error", error);
        }
    );    
  }

  setHistory(){
    this.routeSub = this.route
      .queryParams
      .subscribe(params => {
        if('amount' in params){
          this._conversion = Object.assign(params);
          //this.convert();
          setTimeout(()=>{
            M.updateTextFields();
            this.setValues();
            this.setExchangeRate(this._conversion);
          },100);
        }
    });
  }

  setCurrencies(currencies:IExchangeRate[]) {
    let curobj={};
    this.actualCur = {};
    for(let i=0;i<currencies.length;i++){
      curobj[currencies[i].currency] = null;
      this.actualCur[currencies[i].currency] = currencies[i].rate;
    }
    this.currencies = {
      data: curobj,
    };
    this.setActive();
  }

  onFromChange(event){
    this._conversion.from = event.target.value;
  }

  onToChange(event){
    this._conversion.to = event.target.value;
  }
  
  swapCurrency(){
    const from = this._conversion.from;
    this._conversion.from = this._conversion.to;
    this._conversion.to = from; 
  }

  convert(){
    this.setValues();
    this._conversion = {...this._conversion, date:this.getDate()};
    if(isNaN(this.fromVal)|| isNaN(this.toVal) || this._conversion.amount === 0){
      M.toast({html: 'Invalid entry!'});
    }
    else{
      this.addHistory(this._conversion);
    }
  }

  setValues(){
    let from = this.actualCur[this._conversion.from];
    this.fromVal = this.actualCur[this._conversion.to]/from;
    this.toVal = from/this.actualCur[this._conversion.to];
  }

  addHistory(conversion:IViewedConversion){
    this.store.dispatch(new AddHistory(conversion));
    this.setExchangeRate(conversion);
    M.toast({html: 'Successfully added to history!'});
  }

  setExchangeRate(conversion:IViewedConversion){
    this.totalunit = conversion.amount;
    this.fromValue = this.fromVal;
    this.toValue = this.toVal;
    this.fromCurrency = conversion.from;
    this.toCurrency = conversion.to;
  }

  getDate(){
    const date = new Date();
    const formattedDate = formatDate(date, 'dd/MM/yyyy', this.locale) +' @ '+formatDate(date, 'H:mm', this.locale);
    return formattedDate;
  }

  setActive(){
    $('.tab').removeClass('active');
    $('.tab:first').addClass('active');
  }

}
