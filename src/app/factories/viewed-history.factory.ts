export const ViewedConversionHistory = function(){
    return {
        get: function(){
            const ls = JSON.parse(localStorage.getItem(this.getUser())); 
            return ls === null ? [] : ls;
        },
        set: function(conversions){
            localStorage.setItem(this.getUser(), JSON.stringify(conversions));
        },
        getUser: function(){
            return localStorage.getItem('LoggedInUser');
        }
    }
}  