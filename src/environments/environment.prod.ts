export const environment = {
  production: true,
  apiUrls: {
    getCurrencies: 'https://api.nomics.com/v1/exchange-rates?key=df7a0fe9389f90ee1937ab8b06ef216d',
    getExchangeRateHistory: 'https://api.nomics.com/v1/exchange-rates/history?key=df7a0fe9389f90ee1937ab8b06ef216d'
  }
};
