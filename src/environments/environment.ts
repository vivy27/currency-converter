// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiUrls: {
    getCurrencies: 'https://api.nomics.com/v1/exchange-rates?key=df7a0fe9389f90ee1937ab8b06ef216d',
    getExchangeRateHistory: 'https://api.nomics.com/v1/exchange-rates/history?key=df7a0fe9389f90ee1937ab8b06ef216d'
  }
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
